***********************************************************
*                         NoProb                          *
*                 Ported by: Brad Landis                  *
*                 Maintained by: Eric Knibbe              *
*                                                         *
*   The original theme was taken from oswd.org, and was   *
*   designed by kpgururaja.                               *
*   http://www.oswd.org/design/information/id/2569        *
***********************************************************

-------------
   Summary
-------------

This is a fixed-width design with a single-colored banner and a 
muted colour scheme. It features support for sidebars on one or 
both sides, a print style sheet, and under Drupal 6 and above, a 
menu bar with drop-down menus for nested links. Its text rendering 
has been optimized for consistency and vertical rhythm, and it 
fits browser windows sized at least 900px wide.

Passes validation for XHTML 1.0 and CSS level 3.

-------------------
   Installation
-------------------

1. Make sure you have the phpTemplate theme engine installed. 
   It's installed by default under Drupal 5 and above.

2. Move the noprob folder into your themes folder.

3. Enable NoProb by going to Administer > Site Building > Themes.

-------------------
   Customization
-------------------

By default, nested links in the menu bar will appear as drop-down
menus. You need only arrange your menus in Administer > Site 
Building > Menus, and the code will take care of the rest. If you 
need to turn off drop-down menu generation entirely, open 
page.tpl.php in a text editor and replace all occurrences of the 
word 'noprob' with 'menu'; this will prevent the theme from calling 
the custom functions listed in template.php. 

A change from older releases of this theme is that secondary links 
now appear at the bottom of the page among the footer content as 
a bar of text links, a popular convention, instead of up 
on the right-hand side of the menu bar near the top of the page. 
However, it's still possible to put links at the top right by going 
to the Menus admin page and creating a new one with a menu name of 
"tertiary-links", without the quotes. Drop-down menus will also be 
available for that menu. 

-------------
   Contact
-------------

* Eric Knibbe (Eric3) - http://drupal.org/user/69309

