<?php

/**
 * @file
 * NoProb template file for comments.
 */
?>

<div class="comment<?php print($comment->new) ? ' comment-new' : ''; print ' '. $status ?> clear-block">
  <?php print $picture ?>
  <h3><?php print $title ?></h3>
  <?php if ($comment->new) { ?>
    <span class="new"><?php print $new ?></span>
  <?php } ?>
  <div class="content">
    <?php print $content ?>
    <?php if ($signature) { ?>
      <div class="signature clear-block">
        <?php print $signature ?>
      </div>
    <?php } ?>
  </div>
  <div class="meta clear-block">
    <div class="meta-links"><?php print $links ?></div>
    <div class="submitted">
      <?php //print $submitted ?>
      <?php print t('!date | !username', array('!username' => theme('username', $comment), '!date' => format_date($comment->timestamp))) ?>
    </div>
  </div>
</div>
