<?php

/**
 * @file
 * NoProb main template file.
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?></script>
</head>
<?php flush(); ?>

<body>
<div id="wrapper">

<div id="banner" class="clear-block">
  <?php print $search_box ?>
  <?php if ($logo) { ?>
    <a href="<?php print $front_page ?>" title="<?php print t('Home') ?>">
      <img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" class="logo" />
    </a>
  <?php } ?>
  <?php if ($site_name) { ?>
    <h1 class="site-name"><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
  <?php } ?>
  <?php if ($site_slogan) { ?>
    <div class="site-slogan"><?php print $site_slogan ?></div>
  <?php } ?>
</div>

<div id="menu">
  <?php if (isset($primary_links)) { ?>
    <?php print theme('links', noprob_primary_links(), array('class' => 'links', 'id' => 'primary-links')) ?>
  <?php } ?>
  <?php if (count(noprob_navigation_links('menu-tertiary-links'))) { ?>
    <?php print theme('links', noprob_navigation_links('menu-tertiary-links'), array('class' => 'links', 'id' => 'tertiary-links')) ?>
  <?php } ?>
</div>

<?php if ($header) { ?>
  <div id="header"><?php print $header ?></div>
<?php } ?>

<table id="middle" border="0" cellpadding="0" cellspacing="0">
<tr>
  <?php if ($left) { ?>
    <td id="sidebar-left" class="sidebar"><?php print $left ?></td>
  <?php } ?>

  <td id="main" class="content-<?php print $layout ?>">
    <?php print $breadcrumb ?>
    <?php if ($title) { ?>
      <h2 class="title"><?php print $title ?></h2>
    <?php } ?>
    <?php if ($tabs) { ?>
      <div class="tabs"><?php print $tabs ?></div>
    <?php } ?>
    <?php if ($mission) { ?>
      <div id="mission"><?php print $mission ?></div>
    <?php } ?>
<!-- START main content -->
    <?php if ($show_messages && $messages) print $messages; ?>
    <?php print $help ?>
    <?php print $content ?>
<!-- END main content -->
    <?php print $feed_icons ?>
  </td>

  <?php if ($right) { ?>
    <td id="sidebar-right" class="sidebar"><?php print $right ?></td>
  <?php } ?>
</tr>
</table>

<div id="footer">
  <?php if ($secondary_links || $footer || $footer_message) { ?>
    <?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'secondary-links')) . $footer . $footer_message ?>
  <?php } ?>
</div>

</div>
<?php print $closure ?>
</body>
</html>
